SOBRE
O projeto foi desenvolvido utilizando Java 8, tem com suas principais tecnologias o Spring Boot e Maven, sua
arquitetura foi baseada nos padrões do Spring, que facilitam no  desenvolvimento de aplicações REST.
Para rodar o projeto será necessário o Java 8, Maven e Postgress SQL 9 instalados e JPA metamodel configurado
 no projeto,por ser um projeto Spring Boot não necessita do Apache Tomcat  instalado.

PS: em teoria para rodar este projeto bastaria apenas executar o arquivo “mvnw.cmd” porem nem sempre funciona
comigo, então o projeto foi testado na IDE Eclipse, seguindo os passos abaixo!

PASSOS:
1 = Configurar as propriedades de banco em “application.properties”.
2 = Criar a database com mesmo nome definindo no arquivo “application.properties”.
3 = Importar as dependências do maven.
4 = Configurar  o JPA metamodel.
5 = Executar o arquivo AssembleiaApiApplication.java  (Rum As /java apllication).
6 = Rodar o SQL “arquivos/assembleia.sql” na database criada.
7 = Via Postman importar o arquivo “arquivos/assembleia.postman_collection.json”, para testar as requisições.


VERSIONAMENTO
Versionamento da API, o ideal e sempre manter a compatibilidade das versões nas URIs disponibilizadas, porem 
quando não for possível pode-se criar URIs sugestivas para que o usuário do serviço possa saber qual versão 
ele esta usando, e quando modificações forem lançadas prover o acesso a documentação para o cliente!
