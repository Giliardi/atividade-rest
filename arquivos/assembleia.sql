START TRANSACTION;



INSERT INTO assembleia(ativo, criacao, modificacao, data, descricao, titulo)VALUES ( TRUE, current_timestamp, current_timestamp, current_timestamp,'Assembleia de teste 1', 'Assembleia 1');
INSERT INTO assembleia(ativo, criacao, modificacao, data, descricao, titulo)VALUES ( TRUE, current_timestamp, current_timestamp, current_timestamp,'Assembleia de teste 2', 'Assembleia 2');
INSERT INTO assembleia(ativo, criacao, modificacao, data, descricao, titulo)VALUES ( TRUE, current_timestamp, current_timestamp, current_timestamp,'Assembleia de teste 3', 'Assembleia 3');
INSERT INTO assembleia(ativo, criacao, modificacao, data, descricao, titulo)VALUES ( TRUE, current_timestamp, current_timestamp, current_timestamp,'Assembleia de teste 4', 'Assembleia 4');

INSERT INTO associado(ativo, criacao, modificacao, cpf, mumero_associado, nome) VALUES (true, current_timestamp, null,'17880149015',123456,'Associado 1');
INSERT INTO associado(ativo, criacao, modificacao, cpf, mumero_associado, nome) VALUES (true, current_timestamp, null,'49905770003',123457,'Associado 2');
INSERT INTO associado(ativo, criacao, modificacao, cpf, mumero_associado, nome) VALUES (true, current_timestamp, null,'59525201031',123458,'Associado 3');
INSERT INTO associado(ativo, criacao, modificacao, cpf, mumero_associado, nome) VALUES (true, current_timestamp, null,'24729238052',123459,'Associado 4');
INSERT INTO associado(ativo, criacao, modificacao, cpf, mumero_associado, nome) VALUES (true, current_timestamp, null,'00000000001',123460,'Associado 5');

INSERT INTO sessao( ativo, criacao, modificacao, duracao_em_minuto, inicio, titulo) VALUES ( true, current_timestamp, null, 60, current_timestamp, 'Sessão 1');
INSERT INTO sessao( ativo, criacao, modificacao, duracao_em_minuto, inicio, titulo) VALUES ( true, current_timestamp, null, 1440, current_timestamp, 'Sessão 2');
INSERT INTO sessao( ativo, criacao, modificacao, duracao_em_minuto, inicio, titulo) VALUES ( true, current_timestamp, null, 43200, current_timestamp, 'Sessão 3');
INSERT INTO sessao( ativo, criacao, modificacao, duracao_em_minuto, inicio, titulo) VALUES ( true, current_timestamp, null, 15768000, current_timestamp, 'Sessão 4');

INSERT INTO pauta( ativo, criacao, modificacao, enunciado, titulo, assembleia_id, sessao_id) VALUES ( true, current_timestamp, null, 'Pauta para votacao 1', 'Pauta 1', 1,1);
INSERT INTO pauta( ativo, criacao, modificacao, enunciado, titulo, assembleia_id, sessao_id) VALUES ( true, current_timestamp, null, 'Pauta para votacao 2', 'Pauta 2', 2,2);
INSERT INTO pauta( ativo, criacao, modificacao, enunciado, titulo, assembleia_id, sessao_id) VALUES ( true, current_timestamp, null, 'Pauta para votacao 3', 'Pauta 3', 3,3);
INSERT INTO pauta( ativo, criacao, modificacao, enunciado, titulo, assembleia_id, sessao_id) VALUES ( true, current_timestamp, null, 'Pauta para votacao 4', 'Pauta 4', 4,4);

COMMIT;