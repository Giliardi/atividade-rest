package com.assembleia.api.model.dto;

public class VotacaoDTO {

	private String voto;
	private Long quantidade;
	private Long idPauta;

	public VotacaoDTO() {
		super();
	}

	public VotacaoDTO(String voto, Long quantidade, Long idPauta) {
		super();
		this.voto = voto;
		this.quantidade = quantidade;
		this.idPauta = idPauta;
	}

	public String getVoto() {
		return voto;
	}

	public void setVoto(String voto) {
		this.voto = voto;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public Long getIdPauta() {
		return idPauta;
	}

	public void setIdPauta(Long idPauta) {
		this.idPauta = idPauta;
	}

}
