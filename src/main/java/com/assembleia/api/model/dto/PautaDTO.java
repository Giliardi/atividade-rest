package com.assembleia.api.model.dto;

public class PautaDTO {

	private String titulo;
	private String enunciado;
	private Long idAssembleia;
	private Long idSessao;

	public PautaDTO() {
		super();
	}

	public PautaDTO(String titulo, String enunciado, Long idAssembleia, Long idSessao) {
		super();
		this.titulo = titulo;
		this.enunciado = enunciado;
		this.idAssembleia = idAssembleia;
		this.idSessao = idSessao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public Long getIdAssembleia() {
		return idAssembleia;
	}

	public void setIdAssembleia(Long idAssembleia) {
		this.idAssembleia = idAssembleia;
	}

	public Long getIdSessao() {
		return idSessao;
	}

	public void setIdSessao(Long idSessao) {
		this.idSessao = idSessao;
	}

}
