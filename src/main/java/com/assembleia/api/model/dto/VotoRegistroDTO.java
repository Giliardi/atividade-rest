package com.assembleia.api.model.dto;

import java.util.Date;

public class VotoRegistroDTO {

	private Date data;
	private Long idPauta;
	private Long idAssociado;

	public VotoRegistroDTO() {
		super();
	}

	public VotoRegistroDTO(Date data, Long idPauta, Long idAssociado) {
		super();
		this.data = data;
		this.idPauta = idPauta;
		this.idAssociado = idAssociado;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getIdPauta() {
		return idPauta;
	}

	public void setIdPauta(Long idPauta) {
		this.idPauta = idPauta;
	}

	public Long getIdAssociado() {
		return idAssociado;
	}

	public void setIdAssociado(Long idAssociado) {
		this.idAssociado = idAssociado;
	}

}
