package com.assembleia.api.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SessaoDTO {

	private String titulo;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
	private Date inicio;
	
	private Integer duracaoEmMinuto;

	public SessaoDTO() {
		super();
	}

	public SessaoDTO(String titulo, Date inicio, Integer duracaoEmMinuto) {
		super();
		this.titulo = titulo;
		this.inicio = inicio;
		this.duracaoEmMinuto = duracaoEmMinuto;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Integer getDuracaoEmMinuto() {
		return duracaoEmMinuto;
	}

	public void setDuracaoEmMinuto(Integer duracaoEmMinuto) {
		this.duracaoEmMinuto = duracaoEmMinuto;
	}

}
