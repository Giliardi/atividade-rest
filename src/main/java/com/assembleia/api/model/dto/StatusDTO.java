package com.assembleia.api.model.dto;

public class StatusDTO {

	private String status;

	public StatusDTO() {
		super();
	}

	public StatusDTO(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
