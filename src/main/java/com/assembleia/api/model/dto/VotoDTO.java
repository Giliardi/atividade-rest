package com.assembleia.api.model.dto;

public class VotoDTO {

	private Long idAssociado;
	private Long idPauta;
	private String voto;

	public VotoDTO() {
		super();
	}

	public VotoDTO(Long idAssociado, Long idPauta, String voto) {
		super();
		this.idAssociado = idAssociado;
		this.idPauta = idPauta;
		this.voto = voto;
	}

	public Long getIdAssociado() {
		return idAssociado;
	}

	public void setIdAssociado(Long idAssociado) {
		this.idAssociado = idAssociado;
	}

	public Long getIdPauta() {
		return idPauta;
	}

	public void setIdPauta(Long idPauta) {
		this.idPauta = idPauta;
	}

	public String getVoto() {
		return voto;
	}

	public void setVoto(String voto) {
		this.voto = voto;
	}

}
