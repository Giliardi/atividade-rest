package com.assembleia.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "pauta")
public class Pauta extends GenericEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String titulo;
	
	@Column(columnDefinition = "TEXT")
	private String enunciado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "assembleia_id", updatable = false)
	private Assembleia assembleia;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sessao_id", updatable = false)
	private Sessao sessao;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pauta")
	private List<Voto> votos;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public Assembleia getAssembleia() {
		return assembleia;
	}

	public void setAssembleia(Assembleia assembleia) {
		this.assembleia = assembleia;
	}

	public Sessao getSessao() {
		return sessao;
	}

	public void setSessao(Sessao sessao) {
		this.sessao = sessao;
	}

	public List<Voto> getVotos() {
		return votos;
	}

	public void setVotos(List<Voto> votos) {
		this.votos = votos;
	}

}
