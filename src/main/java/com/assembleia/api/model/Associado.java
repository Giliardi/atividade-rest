package com.assembleia.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "associado")
public class Associado extends GenericEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;

	private String cpf;

	@Column(name = "mumero_associado")
	private Integer mumeroAssociado;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Integer getMumeroAssociado() {
		return mumeroAssociado;
	}

	public void setMumeroAssociado(Integer mumeroAssociado) {
		this.mumeroAssociado = mumeroAssociado;
	}

}
