package com.assembleia.api.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "sessao")
public class Sessao extends GenericEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String titulo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", locale = "pt-BR", timezone = "Brazil/East")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inicio;

	@Column(name = "duracao_em_minuto")
	private Integer duracaoEmMinuto;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Integer getDuracaoEmMinuto() {
		return duracaoEmMinuto;
	}

	public void setDuracaoEmMinuto(Integer duracaoEmMinuto) {
		this.duracaoEmMinuto = duracaoEmMinuto;
	}

}
