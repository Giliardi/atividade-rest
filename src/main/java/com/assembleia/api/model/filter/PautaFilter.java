package com.assembleia.api.model.filter;

public class PautaFilter {
	
	private String titulo;
	private String enunciado;
	private Long idAssembleia;
	private Long idSessao;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public Long getIdAssembleia() {
		return idAssembleia;
	}

	public void setIdAssembleia(Long idAssembleia) {
		this.idAssembleia = idAssembleia;
	}

	public Long getIdSessao() {
		return idSessao;
	}

	public void setIdSessao(Long idSessao) {
		this.idSessao = idSessao;
	}
}
