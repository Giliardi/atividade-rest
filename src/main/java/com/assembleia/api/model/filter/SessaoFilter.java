package com.assembleia.api.model.filter;

import java.util.Date;

public class SessaoFilter {

	private String titulo;
	private Date inicio;
	private Integer duracaoEmMinuto;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Integer getDuracaoEmMinuto() {
		return duracaoEmMinuto;
	}

	public void setDuracaoEmMinuto(Integer duracaoEmMinuto) {
		this.duracaoEmMinuto = duracaoEmMinuto;
	}
}
