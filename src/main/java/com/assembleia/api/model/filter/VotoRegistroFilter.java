package com.assembleia.api.model.filter;

import java.util.Date;

public class VotoRegistroFilter {

	private Long idAssociado;
	private Long idPauta;
	private Date data;

	public VotoRegistroFilter(Long idAssociado, Long idPauta, Date data) {
		super();
		this.idAssociado = idAssociado;
		this.idPauta = idPauta;
		this.data = data;
	}

	public VotoRegistroFilter() {
		super();
	}

	public Long getIdAssociado() {
		return idAssociado;
	}

	public void setIdAssociado(Long idAssociado) {
		this.idAssociado = idAssociado;
	}

	public Long getIdPauta() {
		return idPauta;
	}

	public void setIdPauta(Long idPauta) {
		this.idPauta = idPauta;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
