package com.assembleia.api.model;

public interface EntityInterface {

    public Long getId();
    public Boolean isAtivo();
    
}
