package com.assembleia.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assembleia.api.model.Associado;
import com.assembleia.api.model.Pauta;
import com.assembleia.api.model.Voto;
import com.assembleia.api.model.dto.StatusDTO;
import com.assembleia.api.model.dto.VotoDTO;
import com.assembleia.api.model.dto.VotoRegistroDTO;
import com.assembleia.api.model.filter.VotoRegistroFilter;
import com.assembleia.api.repository.AssociadoRepository;
import com.assembleia.api.repository.PautaRepository;
import com.assembleia.api.repository.VotoRegistroRepository;
import com.assembleia.api.repository.VotoRepository;
import com.assembleia.api.service.exception.MensagemException;
import com.assembleia.api.service.integration.CpfRestService;

@Service
public class VotoService {

	@Autowired
	private VotoRepository votoRepository;

	@Autowired
	private VotoRegistroService votoRegistroService;

	@Autowired
	private VotoRegistroRepository votoRegistroRepository;

	@Autowired
	private PautaRepository pautaRepository;
	
	@Autowired
	private AssociadoRepository associadoRepository;
	
	@Autowired
	private CpfRestService cpfRestService;

	public VotoDTO salvar(VotoDTO votoDTO) {
		Voto voto = new Voto();
		if (votoDTO.getIdPauta() != null) {
			Pauta pauta = buscarPauta(votoDTO.getIdPauta());
			voto.setPauta(pauta);
			voto.setVoto(votoDTO.getVoto());
			validar(votoDTO);
			votoRepository.save(voto);
			votoRegistroService.salvar(votoDTO);
		}
		return entidadeParaDTO(voto);
	}

	private Pauta buscarPauta(Long id) {
		Optional<Pauta> optional = pautaRepository.findById(id);
		if (!optional.isPresent()) {
			throw new  MensagemException("mensagem.pauta-nao-encontrado");
		}
		return optional.get();
	}
	
	private Associado buscarAssociado(Long id) {
		Optional<Associado> optional = associadoRepository.findById(id);
		if (!optional.isPresent()) {
			throw new MensagemException("mensagem.associado-nao-encontrado");
		}
		return optional.get();
	}
	
	

	private void validar(VotoDTO votoDTO) {

		VotoRegistroFilter votoRegistroFilter = new VotoRegistroFilter(votoDTO.getIdAssociado(), votoDTO.getIdPauta(), null);
		Pauta pauta = buscarPauta(votoDTO.getIdPauta());
		Associado associado = buscarAssociado(votoDTO.getIdAssociado());
		List<VotoRegistroDTO> votos = votoRegistroRepository.filtrar(votoRegistroFilter);

		if (associado != null) {
			StatusDTO statusDTO = cpfRestService.validarCpf(associado.getCpf());
			if (statusDTO != null && statusDTO.getStatus().equalsIgnoreCase("UNABLE_TO_VOTE")) {
				throw new MensagemException("mensagem.inapto_para_voto");
			}
		}
		if (votos != null && !votos.isEmpty()) {
			throw new MensagemException("mensagem.voto-ja-registrado");
		}
		if (!votoDTO.getVoto().equals("Sim") && !votoDTO.getVoto().equals("Não")) {
			throw new MensagemException("mensagem.formato-voto-invalido");
		}
		if (pauta.getSessao() != null && pauta.getSessao().getInicio() != null
				&& pauta.getSessao().getDuracaoEmMinuto() != null) {

			Date dh = new Date();

			if (dh.before(pauta.getSessao().getInicio())) {
				throw new MensagemException("mensagem.votacao-nao-aberta");
			} else {
				Integer diferenca = diferencaEmMinutos(pauta.getSessao().getInicio());
				Integer duracao = pauta.getSessao().getDuracaoEmMinuto();
				if (diferenca > duracao) {
					throw new MensagemException("mensagem.votacao-encerrada");
				}
			}

		}

	}

	private List<VotoDTO> entidadeParaDTO(List<Voto> votos) {
		List<VotoDTO> dtos = new ArrayList<>();
		for (Voto voto : votos) {
			dtos.add(entidadeParaDTO(voto));
		}
		return dtos;
	}

	private VotoDTO entidadeParaDTO(Voto voto) {
		return new VotoDTO(null, voto.getPauta().getId(), voto.getVoto());
	}
	
	private Integer diferencaEmMinutos(Date data) {
		Date dh = new Date();
		Long difMil = dh.getTime() - data.getTime();
		Long difMin = ((difMil * 1) / 1000) / 60;
		return difMin.intValue();
	}

}
