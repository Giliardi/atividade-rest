package com.assembleia.api.service.exception;

@SuppressWarnings("serial")
public class MensagemException extends RuntimeException {
	
	private String mensagem;

	public MensagemException(String mensagem) {
		super();
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}


}