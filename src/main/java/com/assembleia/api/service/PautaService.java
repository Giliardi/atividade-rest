package com.assembleia.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assembleia.api.model.Assembleia;
import com.assembleia.api.model.Pauta;
import com.assembleia.api.model.Sessao;
import com.assembleia.api.model.dto.PautaDTO;
import com.assembleia.api.repository.AssembleiaRepository;
import com.assembleia.api.repository.PautaRepository;
import com.assembleia.api.repository.SessaoRepository;
import com.assembleia.api.service.exception.MensagemException;

@Service
public class PautaService {

	@Autowired
	private PautaRepository pautaRepository;

	@Autowired
	private AssembleiaRepository assembleiaRepository;

	@Autowired
	private SessaoRepository sessaoRepository;

	public PautaDTO salvar(PautaDTO pautaDTO) {
		Pauta pauta = new Pauta();
		if (pautaDTO.getIdAssembleia() != null && pautaDTO.getIdSessao() != null) {
			Assembleia assembleia = buscarAssembleia(pautaDTO.getIdAssembleia());
			Sessao sessao = buscarSessao(pautaDTO.getIdSessao());
			pauta.setAssembleia(assembleia);
			pauta.setSessao(sessao);
			pauta.setTitulo(pautaDTO.getTitulo());
			pauta.setEnunciado(pauta.getEnunciado());
			pautaRepository.save(pauta);
		}
		return entidadeParaDTO(pauta);
	}

	@Transactional
	private Assembleia buscarAssembleia(Long id) {
		Optional<Assembleia> optional = assembleiaRepository.findById(id);
		if (!optional.isPresent()) {
			throw new  MensagemException("mensagem.assembleia-nao-encontrada");
		}
		return optional.get();
	}

	@Transactional
	private Sessao buscarSessao(Long id) {
		Optional<Sessao> optional = sessaoRepository.findById(id);
		if (!optional.isPresent()) {
			throw new  MensagemException("mensagem.sessao-nao-encontrada");
		}
		return optional.get();
	}

	@Transactional
	private List<PautaDTO> entidadeParaDTO(List<Pauta> pautas) {
		List<PautaDTO> dtos = new ArrayList<>();
		for (Pauta pauta : pautas) {
			dtos.add(entidadeParaDTO(pauta));
		}
		return dtos;
	}

	private PautaDTO entidadeParaDTO(Pauta pauta) {
		return new PautaDTO(pauta.getTitulo(), pauta.getEnunciado(), pauta.getAssembleia().getId(),pauta.getSessao().getId());
	}
}
