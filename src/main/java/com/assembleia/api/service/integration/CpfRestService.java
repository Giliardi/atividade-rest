package com.assembleia.api.service.integration;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.assembleia.api.model.dto.StatusDTO;

@Service
public class CpfRestService {
	
	//TODO as vezes o serviço esta se perdendo retornando cpf invalido para um cpf valido!
	public StatusDTO validarCpf(String cpf) {
		String numCPF = cpf.replaceAll("[^0-9]", "");
		try {
			RestTemplate restTemplate = new RestTemplate();
			StatusDTO cpfStatusDTO = restTemplate.getForObject("https://user-info.herokuapp.com/users/" + numCPF,StatusDTO.class);
			return cpfStatusDTO;
		} catch (Exception e) {
			//TODO no momento todo cpf inválido esta retornado 404, não parece um comportamento esperado! (de acordo com texto da atividade)
            return new StatusDTO("UNABLE_TO_VOTE");
		}
	}
	

}
