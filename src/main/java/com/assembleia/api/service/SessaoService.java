package com.assembleia.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assembleia.api.model.Sessao;
import com.assembleia.api.model.dto.SessaoDTO;
import com.assembleia.api.repository.SessaoRepository;

@Service
public class SessaoService {
	
	@Autowired
	private SessaoRepository sessaoRepository;

	public SessaoDTO salvar(SessaoDTO sessaoDTO) {
		Sessao sessao = new Sessao();
		sessao.setTitulo(sessaoDTO.getTitulo());
		sessao.setInicio(sessaoDTO.getInicio());
		sessao.setDuracaoEmMinuto(sessaoDTO.getDuracaoEmMinuto() == null ? 1 : sessaoDTO.getDuracaoEmMinuto() );
		sessaoRepository.save(sessao);
		return entidadeParaDTO(sessao);
	}

	private List<SessaoDTO> entidadeParaDTO(List<Sessao> sessaos) {
		List<SessaoDTO> dtos = new ArrayList<>();
		for (Sessao sessao : sessaos) {
			dtos.add(entidadeParaDTO(sessao));
		}
		return dtos;
	}

	private SessaoDTO entidadeParaDTO(Sessao sessao) {
		return new SessaoDTO(sessao.getTitulo(), sessao.getInicio(), sessao.getDuracaoEmMinuto());
	}
	
	
}
