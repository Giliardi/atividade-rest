package com.assembleia.api.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assembleia.api.model.Associado;
import com.assembleia.api.model.Pauta;
import com.assembleia.api.model.VotoRegistro;
import com.assembleia.api.model.dto.VotoDTO;
import com.assembleia.api.repository.AssociadoRepository;
import com.assembleia.api.repository.PautaRepository;
import com.assembleia.api.repository.VotoRegistroRepository;

@Service
public class VotoRegistroService {

	@Autowired
	private PautaRepository pautaRepository;

	@Autowired
	private AssociadoRepository associadoRepository;

	@Autowired
	private VotoRegistroRepository votoRegistoRepository;

	public void salvar(VotoDTO votoDTO) {
		
		VotoRegistro votoRegistro = new VotoRegistro();
		
			Pauta pauta = buscarPauta(votoDTO.getIdPauta());
			Associado associado = buscarAssociado(votoDTO.getIdAssociado());
			votoRegistro.setPauta(pauta);
			votoRegistro.setAssociado(associado);
			votoRegistro.setData(new Date());
			votoRegistoRepository.save(votoRegistro);

	}

	private Pauta buscarPauta(Long id) {
		Optional<Pauta> optionalPauta = pautaRepository.findById(id);
		if (!optionalPauta.isPresent()) {
			throw new IllegalArgumentException();
		}
		return optionalPauta.get();
	}

	private Associado buscarAssociado(Long id) {
		Optional<Associado> optionalAssociado = associadoRepository.findById(id);
		if (!optionalAssociado.isPresent()) {
			throw new IllegalArgumentException();
		}
		return optionalAssociado.get();
	}

}
