package com.assembleia.api.repository.pauta;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.assembleia.api.model.dto.PautaDTO;
import com.assembleia.api.model.filter.PautaFilter;

public interface PautaRepositoryQuery {

	public List<PautaDTO> filtrar(PautaFilter pautaFilter);

	public Page<PautaDTO> filtrar(PautaFilter pautaFilter, Pageable pageable);

}
