package com.assembleia.api.repository.pauta;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.assembleia.api.model.Assembleia_;
import com.assembleia.api.model.Pauta;
import com.assembleia.api.model.Pauta_;
import com.assembleia.api.model.Sessao_;
import com.assembleia.api.model.dto.PautaDTO;
import com.assembleia.api.model.filter.PautaFilter;

public class PautaRepositoryImpl implements PautaRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<PautaDTO> filtrar(PautaFilter pautaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pauta> criteria = builder.createQuery(Pauta.class);

		Root<Pauta> root = criteria.from(Pauta.class);

		Predicate[] predicates = criarRestricoes(pautaFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Pauta> query = manager.createQuery(criteria);

		List<PautaDTO> dtos = entidadeParaDTO(query.getResultList());

		return dtos;

	}

	@Override
	public Page<PautaDTO> filtrar(PautaFilter pautaFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pauta> criteria = builder.createQuery(Pauta.class);

		Root<Pauta> root = criteria.from(Pauta.class);

		Predicate[] predicates = criarRestricoes(pautaFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Pauta> query = manager.createQuery(criteria);
		adicionarRestricoesPaginacao(query, pageable);

		List<PautaDTO> dtos = entidadeParaDTO(query.getResultList());

		return new PageImpl<>(dtos, pageable, total(pautaFilter));

	}

	private Long total(PautaFilter pautaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Pauta> root = criteria.from(Pauta.class);

		Predicate[] predicates = criarRestricoes(pautaFilter, builder, root);
		criteria.where(predicates);
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	private void adicionarRestricoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistroPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistroPorPagina);

	}

	private Predicate[] criarRestricoes(PautaFilter pautaFilter, CriteriaBuilder builder, Root<Pauta> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(pautaFilter.getTitulo())) {
			predicates.add(	builder.like(builder.lower(root.get(Pauta_.titulo)), "%" + pautaFilter.getTitulo().toLowerCase() + "%"));
		}

		if (pautaFilter.getIdAssembleia() != null) {
			predicates.add(builder.equal(root.get(Pauta_.assembleia).get(Assembleia_.id), pautaFilter.getIdAssembleia()));
		}
		
		if (pautaFilter.getIdSessao() != null) {
			predicates.add(builder.equal(root.get(Pauta_.sessao).get(Sessao_.id), pautaFilter.getIdSessao()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private List<PautaDTO> entidadeParaDTO(List<Pauta> pautas) {
		List<PautaDTO> dtos = new ArrayList<>();
		for (Pauta pauta : pautas) {
			dtos.add(new PautaDTO(pauta.getTitulo(),pauta.getEnunciado(), pauta.getAssembleia().getId(), pauta.getSessao().getId()));
		}
		return dtos;
	}

}
