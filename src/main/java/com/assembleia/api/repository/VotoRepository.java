package com.assembleia.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assembleia.api.model.Voto;
import com.assembleia.api.repository.voto.VotoRepositoryQuery;

public interface VotoRepository extends JpaRepository<Voto, Long>, VotoRepositoryQuery {

}
