package com.assembleia.api.repository.sessao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.assembleia.api.model.dto.SessaoDTO;
import com.assembleia.api.model.filter.SessaoFilter;

public interface SessaoRepositoryQuery {

	public List<SessaoDTO> filtrar(SessaoFilter sessaoFilter);

	public Page<SessaoDTO> filtrar(SessaoFilter sessaoFilter, Pageable pageable);

}
