package com.assembleia.api.repository.sessao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.assembleia.api.model.Sessao;
import com.assembleia.api.model.Sessao_;
import com.assembleia.api.model.dto.SessaoDTO;
import com.assembleia.api.model.filter.SessaoFilter;

public class SessaoRepositoryImpl implements SessaoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<SessaoDTO> filtrar(SessaoFilter sessaoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Sessao> criteria = builder.createQuery(Sessao.class);

		Root<Sessao> root = criteria.from(Sessao.class);

		Predicate[] predicates = criarRestricoes(sessaoFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Sessao> query = manager.createQuery(criteria);

		List<SessaoDTO> dtos = entidadeParaDTO(query.getResultList());

		return dtos;

	}

	@Override
	public Page<SessaoDTO> filtrar(SessaoFilter sessaoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Sessao> criteria = builder.createQuery(Sessao.class);

		Root<Sessao> root = criteria.from(Sessao.class);

		Predicate[] predicates = criarRestricoes(sessaoFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Sessao> query = manager.createQuery(criteria);
		adicionarRestricoesPaginacao(query, pageable);

		List<SessaoDTO> dtos = entidadeParaDTO(query.getResultList());

		return new PageImpl<>(dtos, pageable, total(sessaoFilter));

	}

	private Long total(SessaoFilter sessaoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Sessao> root = criteria.from(Sessao.class);

		Predicate[] predicates = criarRestricoes(sessaoFilter, builder, root);
		criteria.where(predicates);
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	private void adicionarRestricoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistroPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistroPorPagina);

	}

	private Predicate[] criarRestricoes(SessaoFilter sessaoFilter, CriteriaBuilder builder, Root<Sessao> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(sessaoFilter.getTitulo())) {
			predicates.add(	builder.like(builder.lower(root.get(Sessao_.titulo)), "%" + sessaoFilter.getTitulo().toLowerCase() + "%"));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private List<SessaoDTO> entidadeParaDTO(List<Sessao> sessaos) {
		List<SessaoDTO> dtos = new ArrayList<>();
		for (Sessao sessao : sessaos) {
			dtos.add(new SessaoDTO(sessao.getTitulo(),sessao.getInicio(), sessao.getDuracaoEmMinuto()));
		}
		return dtos;
	}

}
