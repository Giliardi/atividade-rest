package com.assembleia.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assembleia.api.model.VotoRegistro;
import com.assembleia.api.repository.votoRegistro.VotoRegistroRepositoryQuery;

public interface VotoRegistroRepository extends JpaRepository<VotoRegistro, Long>, VotoRegistroRepositoryQuery {

}
