package com.assembleia.api.repository.voto;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.assembleia.api.model.dto.VotacaoDTO;
import com.assembleia.api.model.dto.VotoDTO;
import com.assembleia.api.model.filter.VotoFilter;

public interface VotoRepositoryQuery {

	public List<VotoDTO> filtrar(VotoFilter votoFilter);

	public Page<VotoDTO> filtrar(VotoFilter votoFilter, Pageable pageable);
	
	public List<VotacaoDTO> votacao(Long idPauta);

}
