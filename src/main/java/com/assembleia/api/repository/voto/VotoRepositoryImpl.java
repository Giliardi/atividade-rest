package com.assembleia.api.repository.voto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.assembleia.api.model.Pauta_;
import com.assembleia.api.model.Voto;
import com.assembleia.api.model.Voto_;
import com.assembleia.api.model.dto.VotacaoDTO;
import com.assembleia.api.model.dto.VotoDTO;
import com.assembleia.api.model.filter.VotoFilter;

public class VotoRepositoryImpl implements VotoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<VotoDTO> filtrar(VotoFilter votoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Voto> criteria = builder.createQuery(Voto.class);

		Root<Voto> root = criteria.from(Voto.class);

		Predicate[] predicates = criarRestricoes(votoFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Voto> query = manager.createQuery(criteria);

		List<VotoDTO> dtos = entidadeParaDTO(query.getResultList());

		return dtos;

	}

	@Override
	public Page<VotoDTO> filtrar(VotoFilter votoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Voto> criteria = builder.createQuery(Voto.class);

		Root<Voto> root = criteria.from(Voto.class);

		Predicate[] predicates = criarRestricoes(votoFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<Voto> query = manager.createQuery(criteria);
		adicionarRestricoesPaginacao(query, pageable);

		List<VotoDTO> dtos = entidadeParaDTO(query.getResultList());

		return new PageImpl<>(dtos, pageable, total(votoFilter));

	}

	private Long total(VotoFilter votoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Voto> root = criteria.from(Voto.class);

		Predicate[] predicates = criarRestricoes(votoFilter, builder, root);
		criteria.where(predicates);
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public List<VotacaoDTO> votacao(Long idPauta) {

		StringBuilder hql = new StringBuilder();
		hql.append("SELECT NEW com.assembleia.api.model.dto.VotacaoDTO( v.voto, COUNT(v.voto) AS quantidade, p.id AS idPauta) ");
		hql.append("FROM voto v ");
		hql.append("LEFT JOIN v.pauta p ");
		hql.append("WHERE p.id = :idPauta ");
		hql.append("GROUP BY v.voto, p.id ");

		Query query = manager.createQuery(hql.toString());
		query.setParameter("idPauta", idPauta);

		return query.getResultList();

	}

	private void adicionarRestricoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistroPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistroPorPagina);

	}

	private Predicate[] criarRestricoes(VotoFilter votoFilter, CriteriaBuilder builder, Root<Voto> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(votoFilter.getVoto())) {
			predicates.add(	builder.like(builder.lower(root.get(Voto_.voto)), "%" + votoFilter.getVoto().toLowerCase() + "%"));
		}

		if (votoFilter.getIdPauta() != null) {
			predicates.add(builder.equal(root.get(Voto_.pauta).get(Pauta_.id), votoFilter.getIdPauta()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private List<VotoDTO> entidadeParaDTO(List<Voto> votos) {
		List<VotoDTO> dtos = new ArrayList<>();
		for (Voto voto : votos) {
			dtos.add(new VotoDTO(null, voto.getPauta().getId(), voto.getVoto()));
		}
		return dtos;
	}

}
