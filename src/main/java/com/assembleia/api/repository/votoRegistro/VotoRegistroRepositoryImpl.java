package com.assembleia.api.repository.votoRegistro;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.assembleia.api.model.Associado_;
import com.assembleia.api.model.Pauta_;
import com.assembleia.api.model.VotoRegistro;
import com.assembleia.api.model.VotoRegistro_;
import com.assembleia.api.model.dto.VotoRegistroDTO;
import com.assembleia.api.model.filter.VotoRegistroFilter;

public class VotoRegistroRepositoryImpl implements VotoRegistroRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<VotoRegistroDTO> filtrar(VotoRegistroFilter votoRegistroFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<VotoRegistro> criteria = builder.createQuery(VotoRegistro.class);

		Root<VotoRegistro> root = criteria.from(VotoRegistro.class);

		Predicate[] predicates = criarRestricoes(votoRegistroFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<VotoRegistro> query = manager.createQuery(criteria);

		List<VotoRegistroDTO> dtos = entidadeParaDTO(query.getResultList());

		return dtos;

	}

	@Override
	public Page<VotoRegistroDTO> filtrar(VotoRegistroFilter votoRegistroFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<VotoRegistro> criteria = builder.createQuery(VotoRegistro.class);

		Root<VotoRegistro> root = criteria.from(VotoRegistro.class);

		Predicate[] predicates = criarRestricoes(votoRegistroFilter, builder, root);
		criteria.where(predicates);

		TypedQuery<VotoRegistro> query = manager.createQuery(criteria);
		adicionarRestricoesPaginacao(query, pageable);

		List<VotoRegistroDTO> dtos = entidadeParaDTO(query.getResultList());

		return new PageImpl<>(dtos, pageable, total(votoRegistroFilter));

	}

	private Long total(VotoRegistroFilter votoRegistroFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<VotoRegistro> root = criteria.from(VotoRegistro.class);

		Predicate[] predicates = criarRestricoes(votoRegistroFilter, builder, root);
		criteria.where(predicates);
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	private void adicionarRestricoesPaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistroPorPagina = pageable.getPageSize();
		int primeirRegistroDaPagina = paginaAtual * totalRegistroPorPagina;

		query.setFirstResult(primeirRegistroDaPagina);
		query.setMaxResults(totalRegistroPorPagina);

	}

	private Predicate[] criarRestricoes(VotoRegistroFilter votoRegistroFilter, CriteriaBuilder builder, Root<VotoRegistro> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (votoRegistroFilter.getIdPauta() != null) {
			predicates.add(builder.equal(root.get(VotoRegistro_.pauta).get(Pauta_.id), votoRegistroFilter.getIdPauta()));
		}
		
		if (votoRegistroFilter.getIdAssociado() != null) {
			predicates.add(builder.equal(root.get(VotoRegistro_.associado).get(Associado_.id), votoRegistroFilter.getIdAssociado()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private List<VotoRegistroDTO> entidadeParaDTO(List<VotoRegistro> votos) {
		List<VotoRegistroDTO> dtos = new ArrayList<>();
		for (VotoRegistro voto : votos) {
			dtos.add(new VotoRegistroDTO(null,voto.getPauta().getId(), voto.getAssociado().getId()));
		}
		return dtos;
	}

}
