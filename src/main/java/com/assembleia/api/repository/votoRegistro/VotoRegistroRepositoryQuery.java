package com.assembleia.api.repository.votoRegistro;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.assembleia.api.model.dto.VotoRegistroDTO;
import com.assembleia.api.model.filter.VotoRegistroFilter;

public interface VotoRegistroRepositoryQuery {

	public List<VotoRegistroDTO> filtrar(VotoRegistroFilter votoRegistroFilter);

	public Page<VotoRegistroDTO> filtrar(VotoRegistroFilter votoRegistroFilter, Pageable pageable);

}
