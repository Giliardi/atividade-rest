package com.assembleia.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assembleia.api.model.Assembleia;

public interface AssembleiaRepository extends JpaRepository<Assembleia, Long> {

}
