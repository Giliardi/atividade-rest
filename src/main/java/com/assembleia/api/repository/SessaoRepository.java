package com.assembleia.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assembleia.api.model.Sessao;
import com.assembleia.api.repository.sessao.SessaoRepositoryQuery;

public interface SessaoRepository extends JpaRepository<Sessao, Long>, SessaoRepositoryQuery {

}
