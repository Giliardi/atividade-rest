package com.assembleia.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assembleia.api.model.Associado;

public interface AssociadoRepository extends JpaRepository<Associado, Long> {

}
