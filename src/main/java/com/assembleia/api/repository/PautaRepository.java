package com.assembleia.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assembleia.api.model.Pauta;
import com.assembleia.api.repository.pauta.PautaRepositoryQuery;

public interface PautaRepository extends JpaRepository<Pauta, Long>,PautaRepositoryQuery {

}
