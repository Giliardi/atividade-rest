package com.assembleia.api.resource;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assembleia.api.model.dto.PautaDTO;
import com.assembleia.api.model.filter.PautaFilter;
import com.assembleia.api.repository.PautaRepository;
import com.assembleia.api.service.PautaService;
import com.assembleia.api.service.exception.MensagemException;
import com.assembleia.api.util.Erro;

@RestController
@RequestMapping("/pautas")
public class PautaResource {

	@Autowired
	private PautaRepository pautaRepository;

	@Autowired
	private PautaService pautaService;
	
	@Autowired
	private MessageSource messageSource;

	@GetMapping("/todos")
	public List<PautaDTO> pesquisar(PautaFilter pautaFilter) {
		return pautaRepository.filtrar(pautaFilter);
	}
	
	@GetMapping
	public Page<PautaDTO> pesquisar(PautaFilter pautaFilter, Pageable pageable) {
		return pautaRepository.filtrar(pautaFilter, pageable);
	}

	@PostMapping
	public ResponseEntity<PautaDTO> criar(@RequestBody PautaDTO pautaDTO) {
		PautaDTO pauta = pautaService.salvar(pautaDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(pautaDTO);
	}
	
	@ExceptionHandler({MensagemException.class})
	public ResponseEntity<Object> handlePessoaInexistenteOuInativaException(MensagemException ex){
		String mensagemUsuario =   messageSource.getMessage(ex.getMensagem(), null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
}
