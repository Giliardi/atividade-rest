package com.assembleia.api.resource;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assembleia.api.model.dto.SessaoDTO;
import com.assembleia.api.model.filter.SessaoFilter;
import com.assembleia.api.repository.SessaoRepository;
import com.assembleia.api.service.SessaoService;
import com.assembleia.api.service.exception.MensagemException;
import com.assembleia.api.util.Erro;

@RestController
@RequestMapping("/sessoes")
public class SessaoResource {

	@Autowired
	private SessaoRepository sessaoRepository;

	@Autowired
	private SessaoService sessaoService;
	
	@Autowired
	private MessageSource messageSource;

	@GetMapping("/todos")
	public List<SessaoDTO> pesquisar(SessaoFilter sessaoFilter) {
		return sessaoRepository.filtrar(sessaoFilter);
	}
	
	@GetMapping
	public Page<SessaoDTO> pesquisar(SessaoFilter sessaoFilter, Pageable pageable) {
		return sessaoRepository.filtrar(sessaoFilter, pageable);
	}

	@PostMapping
	public ResponseEntity<SessaoDTO> criar(@RequestBody SessaoDTO sessaoDTO) {
		SessaoDTO sessao = sessaoService.salvar(sessaoDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(sessaoDTO);
	}
	
	@ExceptionHandler({MensagemException.class})
	public ResponseEntity<Object> handlePessoaInexistenteOuInativaException(MensagemException ex){
		String mensagemUsuario =   messageSource.getMessage(ex.getMensagem(), null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}
}
