package com.assembleia.api.resource;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.assembleia.api.model.dto.VotacaoDTO;
import com.assembleia.api.model.dto.VotoDTO;
import com.assembleia.api.model.filter.VotoFilter;
import com.assembleia.api.repository.VotoRepository;
import com.assembleia.api.service.VotoService;
import com.assembleia.api.service.exception.MensagemException;
import com.assembleia.api.service.exception.PessoaInexistenteOuInativaException;
import com.assembleia.api.util.Erro;


@RestController
@RequestMapping("/votos")
public class VotoResource {

	@Autowired
	private VotoRepository votoRepository;

	@Autowired
	private VotoService votoService;
	
	@Autowired
	private MessageSource messageSource;

	
	@GetMapping("/todos")
	public List<VotoDTO> pesquisar(VotoFilter votoFilter) {
		return votoRepository.filtrar(votoFilter);
	}
	
	
	@GetMapping
	public Page<VotoDTO> pesquisar(VotoFilter votoFilter, Pageable pageable) {
		return votoRepository.filtrar(votoFilter, pageable);
	}

	@PostMapping
	public ResponseEntity<VotoDTO> criar(@RequestBody VotoDTO votoDTO) {
		VotoDTO voto = votoService.salvar(votoDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(votoDTO);
	}
	
	@GetMapping("/votacao/{idPauta}")
	public List<VotacaoDTO> pesquisar(@PathVariable("idPauta") Long idPauta) {
		return votoRepository.votacao(idPauta);
	}
	
	@ExceptionHandler({MensagemException.class})
	public ResponseEntity<Object> handlePessoaInexistenteOuInativaException(MensagemException ex){
		String mensagemUsuario =   messageSource.getMessage(ex.getMensagem(), null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}

}
